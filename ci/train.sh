#!/usr/bin/env bash

python3 -m pip install awscli

image_name=$(bash ci/get_config_value.sh image_name)
if [ -z "$image_name" ]; then
  echo "The config 'image_name' in .sagify.json must be set. Cannot continue."
  exit 1
fi

experiment_id=$(bash ci/get_config_value.sh experiment_id)
if [ -z "$experiment_id" ]; then
  echo "The config 'experiment_id' in .sagify.json must be set. Cannot continue."
  exit 1
fi

s3_training_data_prefix="s3://d-ew1-ted-ai-ml-data/experiments/$experiment_id"

datetime=$(date '+%Y-%m-%d-%H-%M-%S')
image_url="528719223857.dkr.ecr.eu-west-1.amazonaws.com/ci-temporary-images:$image_name-$CI_PIPELINE_ID"
s3_output_prefix=s3://d-ew1-ted-ai-ml-models/models-ci
metrics="$(cat metrics.json)"

# aws s3 cp "$s3_training_data_prefix/hyperparameters.json" /tmp/hyperparameters.json
hyperparams="$(cat src/sagify_base/local_test/test_dir/input/config/hyperparameters.json)"


job_arn=$(
  aws sagemaker create-training-job \
    --training-job-name "$image_name-$datetime" \
    --hyper-parameters "$hyperparams" \
    --algorithm-specification '{"TrainingImage":"'"$image_url"'","TrainingInputMode":"File","MetricDefinitions":'"$metrics"'}' \
    --role-arn "arn:aws:iam::528719223857:role/sagemaker_notebooks" \
    --input-data-config '{"ChannelName":"training","DataSource":{"S3DataSource":{"S3DataType":"S3Prefix","S3Uri":"'"$s3_training_data_prefix"'","S3DataDistributionType":"FullyReplicated"}}}' \
    --output-data-config "S3OutputPath=$s3_output_prefix" \
    --resource-config "InstanceType=ml.m5.4xlarge,InstanceCount=1,VolumeSizeInGB=30" \
    --stopping-condition "MaxRuntimeInSeconds=86400" \
    --query TrainingJobArn \
    --region eu-west-1 \
    --output text
)

job_name=$(echo "$job_arn" | cut -d/ -f 2)
aws sagemaker wait training-job-completed-or-stopped --region eu-west-1 --training-job-name "$job_name"
aws sagemaker describe-training-job --region eu-west-1 --training-job-name  "$job_name" > job_result.json
